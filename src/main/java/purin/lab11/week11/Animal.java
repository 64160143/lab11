package purin.lab11.week11;

public abstract class Animal {
    private String name;
    private int number;

    public Animal(String name, int number){
        this.name = name;
        this.number = number;
    }

    public String getName(){
        return name;
    }

    public int number(){
        return number;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setNumberOfLeg(int number){
        this.number = number;
    }

    @Override
    public String toString() {
        return "Animal (" + name + ") has " + number + "legs";
    }

    public abstract void eat();
    public abstract void sleep();


    
}
