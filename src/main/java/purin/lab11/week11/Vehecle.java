package purin.lab11.week11;

public abstract class Vehecle {
    private String name;
    private String enginename;

    public Vehecle(String name , String enginename){
        this.name = name;
        this.enginename = enginename;
    }
    
    public String getName(){
        return name;
    }

    public String getengine(){
        return enginename;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setengine(String enginename){
        this.enginename = enginename;
    }
}

