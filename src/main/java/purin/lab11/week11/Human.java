package purin.lab11.week11;

public class Human extends Animal implements Walkable,Swimable,Crawlable {

    public Human(String name) {
        super(name,2);
        
    }

    @Override
    public void Crawl() {
        System.out.println(this.toString() + "crawl");
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + "swim");
        
    }

    @Override
    public void walk() {
        System.out.println(this.toString() + "walk");
        
    }

    @Override
    public void run() {
        System.out.println(this.toString() + "run");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + "eat");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + "sleep");;
        
    }
    @Override
    public String toString() {
        return "Human(" + this.getName()+")";
    }

    
}
