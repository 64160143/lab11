package purin.lab11.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("man");
        bat1.eat();
        bat1.sleep();
        bat1.fly();
        bat1.takeoff();
        bat1.landing();

        Bird bir = new Bird("bobo");
        bir.eat();
        bir.sleep();
        bir.fly();
        bir.takeoff();
        bir.landing();

        Fish fish1 = new Fish("pla");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        Plane plane1 = new Plane("911","sdi engine");
        plane1.fly();
        plane1.takeoff();
        plane1.landing();

        Crocodile croc1 = new Crocodile("peter");
        croc1.Crawl();
        croc1.swim();
        croc1.eat();
        croc1.sleep();

        Snake sna = new Snake("mimi");
        sna.Crawl();
        sna.eat();
        sna.sleep();

        Human hu = new Human("huuu");
        hu.Crawl();
        hu.eat();
        hu.sleep();
        hu.swim();
        hu.walk();
        hu.run();

        Rat ra = new Rat("jidd");
        ra.eat();
        ra.sleep();
        ra.walk();
        ra.run();

        Dog doo = new Dog("hong");
        doo.eat();
        doo.sleep();
        doo.walk();
        doo.run();

        Cat ca = new Cat("meoow");
        ca.eat();
        ca.sleep();
        ca.walk();
        ca.run();

        Submarine sa = new Submarine("too","dumnam");
        sa.swim();




        Flyable[] flyobject = {bat1,plane1,bir};
        for(int i = 0; i<flyobject.length;i++){
            flyobject[i].takeoff();
            flyobject[i].fly();
            flyobject[i].landing();
        }

        Walkable[] walkob = {hu,ca,doo,ra};
        for(int i = 0; i<walkob.length;i++){
            walkob[i].run();
            walkob[i].walk();
        }

        Swimable[] swimob = {fish1,croc1,sa};
        for(int i = 0; i<swimob.length;i++){
            swimob[i].swim();
        }
        
        Crawlable[] craob = {sna,croc1};
        for(int i = 0; i<swimob.length;i++){
            craob[i].Crawl();
        }

    }
}
