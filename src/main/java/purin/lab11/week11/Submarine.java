package purin.lab11.week11;

public class Submarine extends Vehecle implements Swimable {

    public Submarine(String name, String enginename) {
        super(name, enginename);
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + "swim");
    }

    @Override
    public String toString() {
        
       return "Submarine(" + this.getName()+") "+ "engine" + this.getengine();
    }
    
}
